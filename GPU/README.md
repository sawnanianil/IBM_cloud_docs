# How to use the GPU in Watson Studio

Naviage to your instance of Watson Studio using IBM Cloud

<img src="/GPU/Images/1.png" width=1000>

To make sure that GPU is enabled on this isntance of Watson Studio press the plan button on the top left. You should see the check mark on the right associated with the Standard Plan which will allow you to use Nvidia GPU.

<img src="/GPU/Images/2.png" width=1000>

Using the Manage button naviage back to the starting page and press the get started button. Select your project.

<img src="/GPU/Images/4.png" width=1000>

In the assets you can create a new notebook using an environment with a GPU. Press the new notebook button.

<img src="/GPU/Images/5.png" width=1000>

Using the runtime selection select an environment with a GPU. These can be identified by the word GPU or Nvidia.

<img src="/GPU/Images/6.png" width=1000>

After selecting the GPU environment you can create the notebook by giving it a name and pressing create button on the bottom right

<img src="/GPU/Images/7.png" width=1000>

You can also change the environment of an existing notebook to use a GPU.

<img src="/GPU/Images/8 a.png" width=1000>

You may need to unlock the notebook to do this.

<img src="/GPU/Images/8 b.png" width=1000>

You also may need to stop the kernel.

<img src="/GPU/Images/10.png" width=1000>

After doing this you will see the option to change the environment.

<img src="/GPU/Images/11.png" width=1000>

Change the environment to one that uses a GPU identified by the word GPU or Nvidia. Then press the associate button.

<img src="/GPU/Images/12.png" width=1000>

The environment change has already been made. The change will be visible in the asset page after you edit the notebook. A GPU is now enabled in your Watson Studio instance and you may use it as if it was a personal GPU. For more info about the actual usage of the GPU use the notebook provided.

<img src="/GPU/Images/13.png" width=1000>
